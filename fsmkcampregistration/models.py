from django.db import models
from django.contrib.auth.models import User

class Registration(models.Model):

    tracks = (
        ('RoR','Ruby On Rails'),
        ('And','Android'),
        ('IoT','Internet of Things')
    )
    region = (
        ('mandya','Mandya'),
        ('bangalore','Bengaluru'),
        ('mangalore','Mangaluru'),
        ('hassan','Hassan'),
        ('others','Others')
    )
    tshirt = (
        ('s','S'),
        ('m','M'),
        ('l','L'),
        ('xl','XL'),
        ('xxl','XXL')
    )
    laptop=(
	('y','Yes'),
	('n','No'),
	('m','Maybe')
    )
    gender_choices = (
        ('male','Male'),
        ('female','Female'),
        ('others','Others')
    )
    participant_contacted_choices = (
        ('yet to contact','Yet to Contact'),
        ('contacted no response','Contacted and No Response'),
        ('contacted and confirmed','Contacted and Confirmed'),
        ('contacted has questions','Contacted but has questions that we need to answer'),
        ('contacted and no interest','Contacted but is not showing interest'),
        ('others','Others')
    )
    name = models.CharField(max_length=200,verbose_name="Full Name")
    college = models.CharField(max_length=300,verbose_name="College Name")
    year = models.IntegerField(verbose_name="Year of Study")
    dept = models.CharField(max_length=5,verbose_name="Department")
    email = models.EmailField(verbose_name="Participant Email")
    mobile = models.PositiveIntegerField(verbose_name="Participant Contact Number")
    track = models.CharField(max_length=5,verbose_name="Track Registering For",choices=tracks)
    region = models.CharField(max_length=20,verbose_name="Region of \
                              Karnataka",choices=region)
    tshirt = models.CharField(max_length=20,verbose_name="Tshirt Size",choices=tshirt , blank=True, null=True,default='s')
    laptop = models.CharField(max_length=20,verbose_name="Getting Laptop?",choices=laptop,default='y')
    is_paid = models.BooleanField(default=False,verbose_name="Has the participant paid ?")
    payment_receiver = models.CharField(max_length=200,verbose_name="Who received the payment")
    coordinator = models.ForeignKey(User,verbose_name="Who is the coordinator",
                                   on_delete=models.PROTECT)
    transaction_id = models.CharField(max_length=200,verbose_name="FSMK \
                                      transaction ID", blank=True, null=True)
    participant_contacted = models.CharField(max_length=50,choices=participant_contacted_choices,default="yet to contact")
    remarks = models.TextField(blank=True, null=True)
    gender = models.CharField(max_length=20, choices=gender_choices, default="others")

    def __str__(self):
        return self.email

    class Meta:
        ordering = ['-track',]

class Volunteers(models.Model):
    tracks = (
        ('RoR','Ruby On Rails'),
        ('And','Android'),
        ('IoT','Internet of Things'),
        ('logistics','Organizers, Logictics and Speakers come here')
    )
    region = (
        ('mandya','Mandya'),
        ('bangalore','Bengaluru'),
        ('mangalore','Mangaluru'),
        ('hassan','Hassan'),
        ('others','Others')
    )
    tshirt = (
        ('s','S'),
        ('m','M'),
        ('l','L'),
        ('xl','XL'),
        ('xxl','XXL')
    )
    gender_choices = (
        ('male','Male'),
        ('female','Female'),
        ('others','Others')
    )
    volunteer_contacted_choices = (
        ('yet to contact','Yet to Contact'),
        ('contacted no response','Contacted and No Response'),
        ('contacted and confirmed','Contacted and Confirmed'),
        ('contacted has questions','Contacted but has questions that we need to answer'),
        ('contacted and no interest','Contacted but is not showing interest'),
        ('others','Others')
    )

    name = models.CharField(max_length=300,verbose_name="Full Name")
    email = models.EmailField(verbose_name="Enter your Email")
    contact = models.PositiveIntegerField(verbose_name="Mobile Number")
    college = models.CharField(max_length=100, verbose_name="5 character \
                               College Name please xP", blank=True, null=True)
    department = models.CharField(max_length=5,verbose_name="Which Department \
                                 (like CSE, ECE etc)", blank=True, null=True)
    semester = models.PositiveIntegerField(verbose_name="Which \
                                           Semester",blank=True, null=True)
    region = models.CharField(max_length=50,verbose_name="Which Region of \
                              Karnataka", choices=region)

    track = models.CharField(max_length=10,verbose_name="Which track are you \
                             volunteering for", choices=tracks)
    tshirt = models.CharField(max_length=100,verbose_name="What tshirt fits \
                              you ?",choices=tshirt)
    volunteer_contacted = models.CharField(max_length=50,choices=volunteer_contacted_choices,default="yet to contact")
    remarks = models.TextField(blank=True, null=True)
    gender = models.CharField(max_length=20, choices=gender_choices, default="others")

    def __str__(self):
        return self.name
