from django.contrib import admin
from django.db import models
from .models import Registration, Volunteers
from django_admin_listfilter_dropdown.filters import DropdownFilter, ChoiceDropdownFilter


class RegistrationAdmin(admin.ModelAdmin):
    list_display = ['name','email','is_paid','track','region']
    search_fields = ('email','mobile','name')
    list_filter = (
        ('track', ChoiceDropdownFilter),
        ('region', ChoiceDropdownFilter),
        ('laptop', ChoiceDropdownFilter),
        ('region', ChoiceDropdownFilter),
        ('gender', ChoiceDropdownFilter),
        ('is_paid', DropdownFilter),
        ('tshirt',DropdownFilter),
        ('participant_contacted',ChoiceDropdownFilter)
    )
admin.site.register(Registration, RegistrationAdmin)

class VolunteerAdmin(admin.ModelAdmin):
    list_display = ['name','email','track','region']
    search_fields = ('email','mobile','name')
    list_filter = (
        ('track', ChoiceDropdownFilter),
        ('region', ChoiceDropdownFilter),
        ('gender', ChoiceDropdownFilter),
        ('tshirt',ChoiceDropdownFilter),
        ('volunteer_contacted',ChoiceDropdownFilter)

        )
admin.site.register(Volunteers, VolunteerAdmin)
